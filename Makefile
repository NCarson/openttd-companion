
.PHONY: all
all: companion/qrc_resources.py
	
companion/qrc_resources.py: resources.qrc #FIXME
	pyrcc5 -o companion/qrc_resources.py resources.qrc

.PHONY: push
push: anoncommit
	git push origin master


.PHONY: anoncommit
anoncommit:
	git add . &&  git commit -m ... || true
	
