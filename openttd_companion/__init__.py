__title__ = "OpenTTD Companion"
__version__ = "v0.0.1"
__license__ = "GPL v2"
__copyright__ = "© Nate Carson 2023"

__atributions__ = [
    "Paper Icons by Sam Hewitt is licensed under CC-SA-4.0",
]
